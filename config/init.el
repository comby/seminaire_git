
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
(package-refresh-contents)
(package-install 'org)
(package-install 'ox-reveal)
(require 'ox-reveal)
